﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Media.Imaging;

namespace PhotOrganizer
{
    public partial class Form1 : Form
    {
        
        public Form1()
        {
            InitializeComponent();
            
        }
        
        private void Button1_Click(object sender, EventArgs e)
        {
            openFileDialog(textBox1);
        }

        private void TextBox1_Click(object sender, EventArgs e)
        {
            openFileDialog(textBox1);
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            openFileDialog(textBox2);
        }

        private void TextBox2_Click(object sender, EventArgs e)
        {
            openFileDialog(textBox2);
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            string[] allfiles = Directory.GetFiles(textBox2.Text, "*.*", SearchOption.TopDirectoryOnly);
            progressBar1.Visible = true;
            progressBar1.Maximum = allfiles.Length+1;
            progressBar1.Value = 1;
            progressBar1.Step = 1;
            foreach(var file in allfiles)
            {
                FileInfo info = new FileInfo(file);

                if (info.Extension.Contains("jpg") || info.Extension.Contains("jpeg"))
                {
                    FileStream fs = new FileStream(info.FullName, FileMode.Open, FileAccess.Read, FileShare.Read);
                    BitmapSource img = BitmapFrame.Create(fs);
                    BitmapMetadata md = (BitmapMetadata)img.Metadata;
                    label5.Text = md.DateTaken;
                    DateTime dateTime = Convert.ToDateTime(md.DateTaken);
                    string year = dateTime.Year.ToString();
                    string month = dateTime.Month.ToString();
                    string month_text = dateTime.ToString("MMM");
                    string day = dateTime.Day.ToString();
                    // Chekcs to see if the directory is created
                    if (year.Length == 4) { 
                        if (!Directory.Exists(textBox1.Text + '\\' + year + '\\' + month + '_' + month_text + '\\' + day))
                        {
                            Directory.CreateDirectory(textBox1.Text + '\\' + year + '\\' + month + '_' + month_text + '\\' + day);
                        }
                    }
                    else
                    {
                        if (!Directory.Exists(textBox1.Text + '\\' + '_NoYear'))
                        {
                            Directory.CreateDirectory(textBox1.Text + '\\' + '_NoYear');
                        }
                    }
                    //info.CopyTo(textBox1.Text + '\'' + year + '\'' + month + '\'' + day);
                }
                else
                {
                    //label5.Text = info.FullName;
                }
                
                progressBar1.PerformStep();
            }
        }

        public void openFileDialog(TextBox textBox)
        {
            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
            folderBrowserDialog.RootFolder = Environment.SpecialFolder.MyComputer;
            if (Directory.Exists(textBox.Text))
                folderBrowserDialog.SelectedPath = textBox.Text;
            else
                folderBrowserDialog.SelectedPath = Environment.SpecialFolder.MyPictures.ToString();
            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                textBox.Text = folderBrowserDialog.SelectedPath;
            }
            else
            {
                if (!Directory.Exists(textBox1.Text))
                {
                    folderBrowserDialog.SelectedPath = string.Empty;
                    textBox.Text = "Plase select a location to save the files";
                }
            }
            folderBrowserDialog.Dispose();
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {

        }
    }
}
