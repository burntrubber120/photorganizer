﻿using System;
using System.IO;
using System.Windows.Forms;
using dirWatcher;
using System.ServiceProcess;
using System.Linq;

namespace PhotOrganizer
{
    public partial class WelcomeScreen : Form
    {
        public WelcomeScreen()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            Form mod = new Form1();
            mod.Owner = this;
            mod.Show();
            this.Hide();
            //this.Hide();
            //need to create a folder on desktop
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            if (!Directory.Exists(path + '\\' + "PhotOrganizer"))
            {
                Directory.CreateDirectory(path + '\\' + "PhotOrganizer");
            }
            //watch folder on desktop
            var serviceExists = ServiceController.GetServices().Any(s => s.ServiceName == "Service1");
            Console.WriteLine(serviceExists);
            serviceExists = ServiceController.GetServices().Any(s => s.ServiceName == "dirWatcher");
            Console.WriteLine(serviceExists);
            serviceExists = ServiceController.GetServices().Any(s => s.ServiceName == "Program");
            Console.WriteLine(serviceExists);
            serviceExists = ServiceController.GetServices().Any(s => s.DisplayName == "Service1");
            Console.WriteLine(serviceExists);
            serviceExists = ServiceController.GetServices().Any(s => s.DisplayName == "dirWatcher");
            Console.WriteLine(serviceExists);
            serviceExists = ServiceController.GetServices().Any(s => s.DisplayName == "Program");
            Console.WriteLine(serviceExists);
            if (!serviceExists)
            {
                Service1 service1 = new Service1();
                service1.InitializeLifetimeService();
                
                serviceExists = ServiceController.GetServices().Any(s => s.ServiceName == service1.ServiceName);
                Console.WriteLine(serviceExists);
                ServiceController service = new ServiceController();
                string[] args = new string[1];
                args[0] = path;
                service.ServiceName = service1.ServiceName;//As it appears in services.msc
                //service.Start(args);
            }
            //new Watcher(path);

            //need to create a folder in Pictures
            path = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);
            if (!Directory.Exists(path + '\\' + "PhotOrganizer"))
            {
                Directory.CreateDirectory(path + '\\' + "PhotOrganizer");
            }

            
        }
    }
}
